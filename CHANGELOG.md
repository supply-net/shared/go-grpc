# CHANGELOG

<!--- next entry here -->

## 1.9.0
2020-01-15

### Features

- add version to each security service (2803dbed16939c65e8b4103dee433e6bf4dafd07)

## 1.8.1
2019-12-29

### Fixes

- add missed field (8f34cd34fd1b1957ecd9f1c5915a741ccc3dd03d)

## 1.8.0
2019-12-29

### Features

- add interfaces and client for transport-order (e2163b789485b6c2cc9a335aff59ed19edfaec8f)

## 1.7.0
2019-12-25

### Features

- rename search class and change field for search on file service (bc58367cc231cd5da925ad0a16ea468545dfc7f0)

## 1.6.0
2019-12-23

### Features

- add search by name for scheme (18484f79068a83436334ef02209a0e2fd86252eb)

## 1.5.0
2019-12-23

### Features

- add search by id for entity and scheme (14215d47e28d3bc4030c35bd2c943884a3edb401)

### Fixes

- update ci (1f81e73684a7fec48c4ec59043953693067eeb23)

## 1.4.3
2019-12-23

### Fixes

- remove stream (adce151676596ce44a621011ea0588c69f965a69)

## 1.4.2
2019-12-23

### Fixes

- fix type for registry-file (3e24c3acf9c6fd0fa1959a48bda80c1327bb56cf)

## 1.4.1
2019-12-23

### Fixes

- remove not used models (8b141c31cd6541c5c92b3a49d79fb3091a87d896)
- fix type for registry-file (5d2a48f3358e5421f4d850b163109bad8d6d2c0a)

## 1.4.0
2019-12-23

### Features

- add services for import (5d249f20d28ecf5589aa1acc86410259d0303766)
- update import service dictionary (d7a42f3986bf9aa06eb376c52608b914f164d54c)

## 1.3.0
2019-12-14

### Features

- move permissions to function get available companies (88f88cbd0f8a10d8e4408b6b6dda231ff0014706)

## 1.2.0
2019-12-13

### Features

- update UserInfo and AvailableCompany models (move permissions from first into second models) (cc81e006e2eaba13db3a41fa85a978b386200ee8)

## 1.1.0
2019-12-11

### Features

- review models and add method for receiving available companies for user (38a8c1c7981076e9b0b6d41d7516309f12df1e70)
- CHANGELOG.md (c8430418875100c805f2de8dbd9197bf8ff8e718)
- update CI (a18bda77606ad9ca985bfde75f8f9054da102fff)
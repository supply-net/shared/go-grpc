// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.21.0
// 	protoc        v3.11.4
// source: common/point.proto

package common

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type Point struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Lat        float64  `protobuf:"fixed64,2,opt,name=Lat,proto3" json:"Lat,omitempty"`
	Lon        float64  `protobuf:"fixed64,3,opt,name=Lon,proto3" json:"Lon,omitempty"`
	LocationID string   `protobuf:"bytes,4,opt,name=locationID,proto3" json:"locationID,omitempty"`
	Address    *Address `protobuf:"bytes,5,opt,name=address,proto3" json:"address,omitempty"`
}

func (x *Point) Reset() {
	*x = Point{}
	if protoimpl.UnsafeEnabled {
		mi := &file_common_point_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Point) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Point) ProtoMessage() {}

func (x *Point) ProtoReflect() protoreflect.Message {
	mi := &file_common_point_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Point.ProtoReflect.Descriptor instead.
func (*Point) Descriptor() ([]byte, []int) {
	return file_common_point_proto_rawDescGZIP(), []int{0}
}

func (x *Point) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *Point) GetLat() float64 {
	if x != nil {
		return x.Lat
	}
	return 0
}

func (x *Point) GetLon() float64 {
	if x != nil {
		return x.Lon
	}
	return 0
}

func (x *Point) GetLocationID() string {
	if x != nil {
		return x.LocationID
	}
	return ""
}

func (x *Point) GetAddress() *Address {
	if x != nil {
		return x.Address
	}
	return nil
}

var File_common_point_proto protoreflect.FileDescriptor

var file_common_point_proto_rawDesc = []byte{
	0x0a, 0x12, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x12, 0x06, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x1a, 0x14, 0x63, 0x6f,
	0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x22, 0x86, 0x01, 0x0a, 0x05, 0x50, 0x6f, 0x69, 0x6e, 0x74, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x10, 0x0a, 0x03,
	0x4c, 0x61, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x01, 0x52, 0x03, 0x4c, 0x61, 0x74, 0x12, 0x10,
	0x0a, 0x03, 0x4c, 0x6f, 0x6e, 0x18, 0x03, 0x20, 0x01, 0x28, 0x01, 0x52, 0x03, 0x4c, 0x6f, 0x6e,
	0x12, 0x1e, 0x0a, 0x0a, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x49, 0x44, 0x18, 0x04,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x49, 0x44,
	0x12, 0x29, 0x0a, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x0f, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x41, 0x64, 0x64, 0x72, 0x65,
	0x73, 0x73, 0x52, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x42, 0x64, 0x0a, 0x0a, 0x63,
	0x6f, 0x6d, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x42, 0x05, 0x50, 0x6f, 0x69, 0x6e, 0x74,
	0x50, 0x01, 0x5a, 0x2b, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x73,
	0x75, 0x70, 0x70, 0x6c, 0x79, 0x2d, 0x6e, 0x65, 0x74, 0x2f, 0x73, 0x68, 0x61, 0x72, 0x65, 0x64,
	0x2f, 0x67, 0x6f, 0x2d, 0x67, 0x72, 0x70, 0x63, 0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0xa2,
	0x02, 0x03, 0x4d, 0x58, 0x58, 0xaa, 0x02, 0x10, 0x53, 0x75, 0x70, 0x70, 0x6c, 0x79, 0x4e, 0x65,
	0x74, 0x2e, 0x43, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0xca, 0x02, 0x06, 0x43, 0x6f, 0x6d, 0x6d, 0x6f,
	0x6e, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_common_point_proto_rawDescOnce sync.Once
	file_common_point_proto_rawDescData = file_common_point_proto_rawDesc
)

func file_common_point_proto_rawDescGZIP() []byte {
	file_common_point_proto_rawDescOnce.Do(func() {
		file_common_point_proto_rawDescData = protoimpl.X.CompressGZIP(file_common_point_proto_rawDescData)
	})
	return file_common_point_proto_rawDescData
}

var file_common_point_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_common_point_proto_goTypes = []interface{}{
	(*Point)(nil),   // 0: common.Point
	(*Address)(nil), // 1: common.Address
}
var file_common_point_proto_depIdxs = []int32{
	1, // 0: common.Point.address:type_name -> common.Address
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_common_point_proto_init() }
func file_common_point_proto_init() {
	if File_common_point_proto != nil {
		return
	}
	file_common_address_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_common_point_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Point); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_common_point_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_common_point_proto_goTypes,
		DependencyIndexes: file_common_point_proto_depIdxs,
		MessageInfos:      file_common_point_proto_msgTypes,
	}.Build()
	File_common_point_proto = out.File
	file_common_point_proto_rawDesc = nil
	file_common_point_proto_goTypes = nil
	file_common_point_proto_depIdxs = nil
}

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.21.0
// 	protoc        v3.11.4
// source: employee/model/search-place.proto

package model

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type SearchPlace struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id    int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Text  string   `protobuf:"bytes,2,opt,name=text,proto3" json:"text,omitempty"`
	Roles []string `protobuf:"bytes,3,rep,name=roles,proto3" json:"roles,omitempty"`
}

func (x *SearchPlace) Reset() {
	*x = SearchPlace{}
	if protoimpl.UnsafeEnabled {
		mi := &file_employee_model_search_place_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SearchPlace) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SearchPlace) ProtoMessage() {}

func (x *SearchPlace) ProtoReflect() protoreflect.Message {
	mi := &file_employee_model_search_place_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SearchPlace.ProtoReflect.Descriptor instead.
func (*SearchPlace) Descriptor() ([]byte, []int) {
	return file_employee_model_search_place_proto_rawDescGZIP(), []int{0}
}

func (x *SearchPlace) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *SearchPlace) GetText() string {
	if x != nil {
		return x.Text
	}
	return ""
}

func (x *SearchPlace) GetRoles() []string {
	if x != nil {
		return x.Roles
	}
	return nil
}

var File_employee_model_search_place_proto protoreflect.FileDescriptor

var file_employee_model_search_place_proto_rawDesc = []byte{
	0x0a, 0x21, 0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c,
	0x2f, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x2d, 0x70, 0x6c, 0x61, 0x63, 0x65, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x12, 0x08, 0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x22, 0x47, 0x0a,
	0x0b, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x50, 0x6c, 0x61, 0x63, 0x65, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04,
	0x74, 0x65, 0x78, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x65, 0x78, 0x74,
	0x12, 0x14, 0x0a, 0x05, 0x72, 0x6f, 0x6c, 0x65, 0x73, 0x18, 0x03, 0x20, 0x03, 0x28, 0x09, 0x52,
	0x05, 0x72, 0x6f, 0x6c, 0x65, 0x73, 0x42, 0x94, 0x01, 0x0a, 0x22, 0x63, 0x6f, 0x6d, 0x2e, 0x73,
	0x75, 0x70, 0x70, 0x6c, 0x79, 0x2d, 0x6e, 0x65, 0x74, 0x2e, 0x67, 0x72, 0x70, 0x63, 0x2e, 0x65,
	0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x2e, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x42, 0x0b, 0x53,
	0x65, 0x61, 0x72, 0x63, 0x68, 0x50, 0x6c, 0x61, 0x63, 0x65, 0x50, 0x01, 0x5a, 0x33, 0x67, 0x69,
	0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x79, 0x2d,
	0x6e, 0x65, 0x74, 0x2f, 0x73, 0x68, 0x61, 0x72, 0x65, 0x64, 0x2f, 0x67, 0x6f, 0x2d, 0x67, 0x72,
	0x70, 0x63, 0x2f, 0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x2f, 0x6d, 0x6f, 0x64, 0x65,
	0x6c, 0xa2, 0x02, 0x03, 0x4d, 0x58, 0x58, 0xaa, 0x02, 0x18, 0x53, 0x75, 0x70, 0x70, 0x6c, 0x79,
	0x4e, 0x65, 0x74, 0x2e, 0x45, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x2e, 0x4d, 0x6f, 0x64,
	0x65, 0x6c, 0xca, 0x02, 0x08, 0x45, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x62, 0x06, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_employee_model_search_place_proto_rawDescOnce sync.Once
	file_employee_model_search_place_proto_rawDescData = file_employee_model_search_place_proto_rawDesc
)

func file_employee_model_search_place_proto_rawDescGZIP() []byte {
	file_employee_model_search_place_proto_rawDescOnce.Do(func() {
		file_employee_model_search_place_proto_rawDescData = protoimpl.X.CompressGZIP(file_employee_model_search_place_proto_rawDescData)
	})
	return file_employee_model_search_place_proto_rawDescData
}

var file_employee_model_search_place_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_employee_model_search_place_proto_goTypes = []interface{}{
	(*SearchPlace)(nil), // 0: employee.SearchPlace
}
var file_employee_model_search_place_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_employee_model_search_place_proto_init() }
func file_employee_model_search_place_proto_init() {
	if File_employee_model_search_place_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_employee_model_search_place_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SearchPlace); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_employee_model_search_place_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_employee_model_search_place_proto_goTypes,
		DependencyIndexes: file_employee_model_search_place_proto_depIdxs,
		MessageInfos:      file_employee_model_search_place_proto_msgTypes,
	}.Build()
	File_employee_model_search_place_proto = out.File
	file_employee_model_search_place_proto_rawDesc = nil
	file_employee_model_search_place_proto_goTypes = nil
	file_employee_model_search_place_proto_depIdxs = nil
}

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.21.0
// 	protoc        v3.11.4
// source: import/dictionary-service.proto

package _import

import (
	context "context"
	proto "github.com/golang/protobuf/proto"
	common "gitlab.com/supply-net/shared/go-grpc/common"
	model "gitlab.com/supply-net/shared/go-grpc/import/model"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type SearchEntityRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Grid   *common.Grid        `protobuf:"bytes,1,opt,name=grid,proto3" json:"grid,omitempty"`
	Search *model.SearchEntity `protobuf:"bytes,2,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *SearchEntityRequest) Reset() {
	*x = SearchEntityRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_import_dictionary_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SearchEntityRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SearchEntityRequest) ProtoMessage() {}

func (x *SearchEntityRequest) ProtoReflect() protoreflect.Message {
	mi := &file_import_dictionary_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SearchEntityRequest.ProtoReflect.Descriptor instead.
func (*SearchEntityRequest) Descriptor() ([]byte, []int) {
	return file_import_dictionary_service_proto_rawDescGZIP(), []int{0}
}

func (x *SearchEntityRequest) GetGrid() *common.Grid {
	if x != nil {
		return x.Grid
	}
	return nil
}

func (x *SearchEntityRequest) GetSearch() *model.SearchEntity {
	if x != nil {
		return x.Search
	}
	return nil
}

type SearchEntityResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data []*model.Entity `protobuf:"bytes,1,rep,name=data,proto3" json:"data,omitempty"`
}

func (x *SearchEntityResponse) Reset() {
	*x = SearchEntityResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_import_dictionary_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SearchEntityResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SearchEntityResponse) ProtoMessage() {}

func (x *SearchEntityResponse) ProtoReflect() protoreflect.Message {
	mi := &file_import_dictionary_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SearchEntityResponse.ProtoReflect.Descriptor instead.
func (*SearchEntityResponse) Descriptor() ([]byte, []int) {
	return file_import_dictionary_service_proto_rawDescGZIP(), []int{1}
}

func (x *SearchEntityResponse) GetData() []*model.Entity {
	if x != nil {
		return x.Data
	}
	return nil
}

type SearchSchemeRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Grid   *common.Grid        `protobuf:"bytes,1,opt,name=grid,proto3" json:"grid,omitempty"`
	Search *model.SearchScheme `protobuf:"bytes,2,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *SearchSchemeRequest) Reset() {
	*x = SearchSchemeRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_import_dictionary_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SearchSchemeRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SearchSchemeRequest) ProtoMessage() {}

func (x *SearchSchemeRequest) ProtoReflect() protoreflect.Message {
	mi := &file_import_dictionary_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SearchSchemeRequest.ProtoReflect.Descriptor instead.
func (*SearchSchemeRequest) Descriptor() ([]byte, []int) {
	return file_import_dictionary_service_proto_rawDescGZIP(), []int{2}
}

func (x *SearchSchemeRequest) GetGrid() *common.Grid {
	if x != nil {
		return x.Grid
	}
	return nil
}

func (x *SearchSchemeRequest) GetSearch() *model.SearchScheme {
	if x != nil {
		return x.Search
	}
	return nil
}

type SearchSchemeResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data []*model.Scheme `protobuf:"bytes,1,rep,name=data,proto3" json:"data,omitempty"`
}

func (x *SearchSchemeResponse) Reset() {
	*x = SearchSchemeResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_import_dictionary_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SearchSchemeResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SearchSchemeResponse) ProtoMessage() {}

func (x *SearchSchemeResponse) ProtoReflect() protoreflect.Message {
	mi := &file_import_dictionary_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SearchSchemeResponse.ProtoReflect.Descriptor instead.
func (*SearchSchemeResponse) Descriptor() ([]byte, []int) {
	return file_import_dictionary_service_proto_rawDescGZIP(), []int{3}
}

func (x *SearchSchemeResponse) GetData() []*model.Scheme {
	if x != nil {
		return x.Data
	}
	return nil
}

var File_import_dictionary_service_proto protoreflect.FileDescriptor

var file_import_dictionary_service_proto_rawDesc = []byte{
	0x0a, 0x1f, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2f, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e,
	0x61, 0x72, 0x79, 0x2d, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x12, 0x06, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x1a, 0x14, 0x63, 0x6f, 0x6d, 0x6d, 0x6f,
	0x6e, 0x2f, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a,
	0x11, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x67, 0x72, 0x69, 0x64, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x1a, 0x19, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c,
	0x2f, 0x65, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x19, 0x69,
	0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x2f, 0x73, 0x63, 0x68, 0x65,
	0x6d, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x20, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74,
	0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x2f, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x2d, 0x65, 0x6e,
	0x74, 0x69, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x20, 0x69, 0x6d, 0x70, 0x6f,
	0x72, 0x74, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x2f, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x2d,
	0x73, 0x63, 0x68, 0x65, 0x6d, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x65, 0x0a, 0x13,
	0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x45, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x12, 0x20, 0x0a, 0x04, 0x67, 0x72, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x0c, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x47, 0x72, 0x69, 0x64, 0x52,
	0x04, 0x67, 0x72, 0x69, 0x64, 0x12, 0x2c, 0x0a, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x14, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x53,
	0x65, 0x61, 0x72, 0x63, 0x68, 0x45, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x52, 0x06, 0x73, 0x65, 0x61,
	0x72, 0x63, 0x68, 0x22, 0x3a, 0x0a, 0x14, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x45, 0x6e, 0x74,
	0x69, 0x74, 0x79, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x22, 0x0a, 0x04, 0x64,
	0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x0e, 0x2e, 0x69, 0x6d, 0x70, 0x6f,
	0x72, 0x74, 0x2e, 0x45, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x22,
	0x65, 0x0a, 0x13, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x53, 0x63, 0x68, 0x65, 0x6d, 0x65, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x20, 0x0a, 0x04, 0x67, 0x72, 0x69, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x0b, 0x32, 0x0c, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x47, 0x72,
	0x69, 0x64, 0x52, 0x04, 0x67, 0x72, 0x69, 0x64, 0x12, 0x2c, 0x0a, 0x06, 0x73, 0x65, 0x61, 0x72,
	0x63, 0x68, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x14, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72,
	0x74, 0x2e, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x53, 0x63, 0x68, 0x65, 0x6d, 0x65, 0x52, 0x06,
	0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x22, 0x3a, 0x0a, 0x14, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68,
	0x53, 0x63, 0x68, 0x65, 0x6d, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x22,
	0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x0e, 0x2e, 0x69,
	0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x53, 0x63, 0x68, 0x65, 0x6d, 0x65, 0x52, 0x04, 0x64, 0x61,
	0x74, 0x61, 0x32, 0xe5, 0x01, 0x0a, 0x11, 0x44, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72,
	0x79, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x3a, 0x0a, 0x07, 0x56, 0x65, 0x72, 0x73,
	0x69, 0x6f, 0x6e, 0x12, 0x16, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x56, 0x65, 0x72,
	0x73, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x17, 0x2e, 0x63, 0x6f,
	0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x56, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x12, 0x49, 0x0a, 0x0c, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x45, 0x6e,
	0x74, 0x69, 0x74, 0x79, 0x12, 0x1b, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x53, 0x65,
	0x61, 0x72, 0x63, 0x68, 0x45, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x1c, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x53, 0x65, 0x61, 0x72, 0x63,
	0x68, 0x45, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x49, 0x0a, 0x0c, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x53, 0x63, 0x68, 0x65, 0x6d, 0x65, 0x12,
	0x1b, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x53,
	0x63, 0x68, 0x65, 0x6d, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1c, 0x2e, 0x69,
	0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x53, 0x63, 0x68, 0x65,
	0x6d, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x80, 0x01, 0x0a, 0x1a, 0x63,
	0x6f, 0x6d, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x79, 0x2d, 0x6e, 0x65, 0x74, 0x2e, 0x67, 0x72,
	0x70, 0x63, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x42, 0x11, 0x44, 0x69, 0x63, 0x74, 0x69,
	0x6f, 0x6e, 0x61, 0x72, 0x79, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x50, 0x01, 0x5a, 0x2b,
	0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x73, 0x75, 0x70, 0x70, 0x6c,
	0x79, 0x2d, 0x6e, 0x65, 0x74, 0x2f, 0x73, 0x68, 0x61, 0x72, 0x65, 0x64, 0x2f, 0x67, 0x6f, 0x2d,
	0x67, 0x72, 0x70, 0x63, 0x2f, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0xa2, 0x02, 0x03, 0x4d, 0x58,
	0x58, 0xaa, 0x02, 0x10, 0x53, 0x75, 0x70, 0x70, 0x6c, 0x79, 0x4e, 0x65, 0x74, 0x2e, 0x49, 0x6d,
	0x70, 0x6f, 0x72, 0x74, 0xca, 0x02, 0x06, 0x49, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x62, 0x06, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_import_dictionary_service_proto_rawDescOnce sync.Once
	file_import_dictionary_service_proto_rawDescData = file_import_dictionary_service_proto_rawDesc
)

func file_import_dictionary_service_proto_rawDescGZIP() []byte {
	file_import_dictionary_service_proto_rawDescOnce.Do(func() {
		file_import_dictionary_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_import_dictionary_service_proto_rawDescData)
	})
	return file_import_dictionary_service_proto_rawDescData
}

var file_import_dictionary_service_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_import_dictionary_service_proto_goTypes = []interface{}{
	(*SearchEntityRequest)(nil),    // 0: import.SearchEntityRequest
	(*SearchEntityResponse)(nil),   // 1: import.SearchEntityResponse
	(*SearchSchemeRequest)(nil),    // 2: import.SearchSchemeRequest
	(*SearchSchemeResponse)(nil),   // 3: import.SearchSchemeResponse
	(*common.Grid)(nil),            // 4: common.Grid
	(*model.SearchEntity)(nil),     // 5: import.SearchEntity
	(*model.Entity)(nil),           // 6: import.Entity
	(*model.SearchScheme)(nil),     // 7: import.SearchScheme
	(*model.Scheme)(nil),           // 8: import.Scheme
	(*common.VersionRequest)(nil),  // 9: common.VersionRequest
	(*common.VersionResponse)(nil), // 10: common.VersionResponse
}
var file_import_dictionary_service_proto_depIdxs = []int32{
	4,  // 0: import.SearchEntityRequest.grid:type_name -> common.Grid
	5,  // 1: import.SearchEntityRequest.search:type_name -> import.SearchEntity
	6,  // 2: import.SearchEntityResponse.data:type_name -> import.Entity
	4,  // 3: import.SearchSchemeRequest.grid:type_name -> common.Grid
	7,  // 4: import.SearchSchemeRequest.search:type_name -> import.SearchScheme
	8,  // 5: import.SearchSchemeResponse.data:type_name -> import.Scheme
	9,  // 6: import.DictionaryService.Version:input_type -> common.VersionRequest
	0,  // 7: import.DictionaryService.SearchEntity:input_type -> import.SearchEntityRequest
	2,  // 8: import.DictionaryService.SearchScheme:input_type -> import.SearchSchemeRequest
	10, // 9: import.DictionaryService.Version:output_type -> common.VersionResponse
	1,  // 10: import.DictionaryService.SearchEntity:output_type -> import.SearchEntityResponse
	3,  // 11: import.DictionaryService.SearchScheme:output_type -> import.SearchSchemeResponse
	9,  // [9:12] is the sub-list for method output_type
	6,  // [6:9] is the sub-list for method input_type
	6,  // [6:6] is the sub-list for extension type_name
	6,  // [6:6] is the sub-list for extension extendee
	0,  // [0:6] is the sub-list for field type_name
}

func init() { file_import_dictionary_service_proto_init() }
func file_import_dictionary_service_proto_init() {
	if File_import_dictionary_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_import_dictionary_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SearchEntityRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_import_dictionary_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SearchEntityResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_import_dictionary_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SearchSchemeRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_import_dictionary_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SearchSchemeResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_import_dictionary_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_import_dictionary_service_proto_goTypes,
		DependencyIndexes: file_import_dictionary_service_proto_depIdxs,
		MessageInfos:      file_import_dictionary_service_proto_msgTypes,
	}.Build()
	File_import_dictionary_service_proto = out.File
	file_import_dictionary_service_proto_rawDesc = nil
	file_import_dictionary_service_proto_goTypes = nil
	file_import_dictionary_service_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// DictionaryServiceClient is the client API for DictionaryService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type DictionaryServiceClient interface {
	Version(ctx context.Context, in *common.VersionRequest, opts ...grpc.CallOption) (*common.VersionResponse, error)
	SearchEntity(ctx context.Context, in *SearchEntityRequest, opts ...grpc.CallOption) (*SearchEntityResponse, error)
	SearchScheme(ctx context.Context, in *SearchSchemeRequest, opts ...grpc.CallOption) (*SearchSchemeResponse, error)
}

type dictionaryServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewDictionaryServiceClient(cc grpc.ClientConnInterface) DictionaryServiceClient {
	return &dictionaryServiceClient{cc}
}

func (c *dictionaryServiceClient) Version(ctx context.Context, in *common.VersionRequest, opts ...grpc.CallOption) (*common.VersionResponse, error) {
	out := new(common.VersionResponse)
	err := c.cc.Invoke(ctx, "/import.DictionaryService/Version", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dictionaryServiceClient) SearchEntity(ctx context.Context, in *SearchEntityRequest, opts ...grpc.CallOption) (*SearchEntityResponse, error) {
	out := new(SearchEntityResponse)
	err := c.cc.Invoke(ctx, "/import.DictionaryService/SearchEntity", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dictionaryServiceClient) SearchScheme(ctx context.Context, in *SearchSchemeRequest, opts ...grpc.CallOption) (*SearchSchemeResponse, error) {
	out := new(SearchSchemeResponse)
	err := c.cc.Invoke(ctx, "/import.DictionaryService/SearchScheme", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DictionaryServiceServer is the server API for DictionaryService service.
type DictionaryServiceServer interface {
	Version(context.Context, *common.VersionRequest) (*common.VersionResponse, error)
	SearchEntity(context.Context, *SearchEntityRequest) (*SearchEntityResponse, error)
	SearchScheme(context.Context, *SearchSchemeRequest) (*SearchSchemeResponse, error)
}

// UnimplementedDictionaryServiceServer can be embedded to have forward compatible implementations.
type UnimplementedDictionaryServiceServer struct {
}

func (*UnimplementedDictionaryServiceServer) Version(context.Context, *common.VersionRequest) (*common.VersionResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Version not implemented")
}
func (*UnimplementedDictionaryServiceServer) SearchEntity(context.Context, *SearchEntityRequest) (*SearchEntityResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SearchEntity not implemented")
}
func (*UnimplementedDictionaryServiceServer) SearchScheme(context.Context, *SearchSchemeRequest) (*SearchSchemeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SearchScheme not implemented")
}

func RegisterDictionaryServiceServer(s *grpc.Server, srv DictionaryServiceServer) {
	s.RegisterService(&_DictionaryService_serviceDesc, srv)
}

func _DictionaryService_Version_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(common.VersionRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DictionaryServiceServer).Version(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/import.DictionaryService/Version",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DictionaryServiceServer).Version(ctx, req.(*common.VersionRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DictionaryService_SearchEntity_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchEntityRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DictionaryServiceServer).SearchEntity(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/import.DictionaryService/SearchEntity",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DictionaryServiceServer).SearchEntity(ctx, req.(*SearchEntityRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DictionaryService_SearchScheme_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchSchemeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DictionaryServiceServer).SearchScheme(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/import.DictionaryService/SearchScheme",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DictionaryServiceServer).SearchScheme(ctx, req.(*SearchSchemeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _DictionaryService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "import.DictionaryService",
	HandlerType: (*DictionaryServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Version",
			Handler:    _DictionaryService_Version_Handler,
		},
		{
			MethodName: "SearchEntity",
			Handler:    _DictionaryService_SearchEntity_Handler,
		},
		{
			MethodName: "SearchScheme",
			Handler:    _DictionaryService_SearchScheme_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "import/dictionary-service.proto",
}

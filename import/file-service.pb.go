// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.21.0
// 	protoc        v3.11.4
// source: import/file-service.proto

package _import

import (
	context "context"
	proto "github.com/golang/protobuf/proto"
	common "gitlab.com/supply-net/shared/go-grpc/common"
	model "gitlab.com/supply-net/shared/go-grpc/import/model"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type BeginUploadRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	EntityId int32  `protobuf:"varint,1,opt,name=entityId,proto3" json:"entityId,omitempty"`
	SchemeId int32  `protobuf:"varint,2,opt,name=schemeId,proto3" json:"schemeId,omitempty"`
	FileName string `protobuf:"bytes,3,opt,name=fileName,proto3" json:"fileName,omitempty"`
}

func (x *BeginUploadRequest) Reset() {
	*x = BeginUploadRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_import_file_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BeginUploadRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BeginUploadRequest) ProtoMessage() {}

func (x *BeginUploadRequest) ProtoReflect() protoreflect.Message {
	mi := &file_import_file_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BeginUploadRequest.ProtoReflect.Descriptor instead.
func (*BeginUploadRequest) Descriptor() ([]byte, []int) {
	return file_import_file_service_proto_rawDescGZIP(), []int{0}
}

func (x *BeginUploadRequest) GetEntityId() int32 {
	if x != nil {
		return x.EntityId
	}
	return 0
}

func (x *BeginUploadRequest) GetSchemeId() int32 {
	if x != nil {
		return x.SchemeId
	}
	return 0
}

func (x *BeginUploadRequest) GetFileName() string {
	if x != nil {
		return x.FileName
	}
	return ""
}

type BeginUploadResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	FileId int64 `protobuf:"varint,2,opt,name=fileId,proto3" json:"fileId,omitempty"`
}

func (x *BeginUploadResponse) Reset() {
	*x = BeginUploadResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_import_file_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BeginUploadResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BeginUploadResponse) ProtoMessage() {}

func (x *BeginUploadResponse) ProtoReflect() protoreflect.Message {
	mi := &file_import_file_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BeginUploadResponse.ProtoReflect.Descriptor instead.
func (*BeginUploadResponse) Descriptor() ([]byte, []int) {
	return file_import_file_service_proto_rawDescGZIP(), []int{1}
}

func (x *BeginUploadResponse) GetFileId() int64 {
	if x != nil {
		return x.FileId
	}
	return 0
}

type ProcessingUploadRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	FileId int64  `protobuf:"varint,1,opt,name=fileId,proto3" json:"fileId,omitempty"`
	Data   []byte `protobuf:"bytes,4,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *ProcessingUploadRequest) Reset() {
	*x = ProcessingUploadRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_import_file_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ProcessingUploadRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ProcessingUploadRequest) ProtoMessage() {}

func (x *ProcessingUploadRequest) ProtoReflect() protoreflect.Message {
	mi := &file_import_file_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ProcessingUploadRequest.ProtoReflect.Descriptor instead.
func (*ProcessingUploadRequest) Descriptor() ([]byte, []int) {
	return file_import_file_service_proto_rawDescGZIP(), []int{2}
}

func (x *ProcessingUploadRequest) GetFileId() int64 {
	if x != nil {
		return x.FileId
	}
	return 0
}

func (x *ProcessingUploadRequest) GetData() []byte {
	if x != nil {
		return x.Data
	}
	return nil
}

type ProcessingUploadResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *ProcessingUploadResponse) Reset() {
	*x = ProcessingUploadResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_import_file_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ProcessingUploadResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ProcessingUploadResponse) ProtoMessage() {}

func (x *ProcessingUploadResponse) ProtoReflect() protoreflect.Message {
	mi := &file_import_file_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ProcessingUploadResponse.ProtoReflect.Descriptor instead.
func (*ProcessingUploadResponse) Descriptor() ([]byte, []int) {
	return file_import_file_service_proto_rawDescGZIP(), []int{3}
}

type CloseUploadRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	FileId int64 `protobuf:"varint,1,opt,name=fileId,proto3" json:"fileId,omitempty"`
}

func (x *CloseUploadRequest) Reset() {
	*x = CloseUploadRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_import_file_service_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CloseUploadRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CloseUploadRequest) ProtoMessage() {}

func (x *CloseUploadRequest) ProtoReflect() protoreflect.Message {
	mi := &file_import_file_service_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CloseUploadRequest.ProtoReflect.Descriptor instead.
func (*CloseUploadRequest) Descriptor() ([]byte, []int) {
	return file_import_file_service_proto_rawDescGZIP(), []int{4}
}

func (x *CloseUploadRequest) GetFileId() int64 {
	if x != nil {
		return x.FileId
	}
	return 0
}

type CloseUploadResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	StatusId int32 `protobuf:"varint,1,opt,name=statusId,proto3" json:"statusId,omitempty"`
}

func (x *CloseUploadResponse) Reset() {
	*x = CloseUploadResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_import_file_service_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CloseUploadResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CloseUploadResponse) ProtoMessage() {}

func (x *CloseUploadResponse) ProtoReflect() protoreflect.Message {
	mi := &file_import_file_service_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CloseUploadResponse.ProtoReflect.Descriptor instead.
func (*CloseUploadResponse) Descriptor() ([]byte, []int) {
	return file_import_file_service_proto_rawDescGZIP(), []int{5}
}

func (x *CloseUploadResponse) GetStatusId() int32 {
	if x != nil {
		return x.StatusId
	}
	return 0
}

type SearchFileRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Grid   *common.Grid      `protobuf:"bytes,1,opt,name=grid,proto3" json:"grid,omitempty"`
	Search *model.SearchFile `protobuf:"bytes,2,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *SearchFileRequest) Reset() {
	*x = SearchFileRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_import_file_service_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SearchFileRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SearchFileRequest) ProtoMessage() {}

func (x *SearchFileRequest) ProtoReflect() protoreflect.Message {
	mi := &file_import_file_service_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SearchFileRequest.ProtoReflect.Descriptor instead.
func (*SearchFileRequest) Descriptor() ([]byte, []int) {
	return file_import_file_service_proto_rawDescGZIP(), []int{6}
}

func (x *SearchFileRequest) GetGrid() *common.Grid {
	if x != nil {
		return x.Grid
	}
	return nil
}

func (x *SearchFileRequest) GetSearch() *model.SearchFile {
	if x != nil {
		return x.Search
	}
	return nil
}

type SearchFileResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data []*model.RegistryFile `protobuf:"bytes,1,rep,name=data,proto3" json:"data,omitempty"`
}

func (x *SearchFileResponse) Reset() {
	*x = SearchFileResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_import_file_service_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SearchFileResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SearchFileResponse) ProtoMessage() {}

func (x *SearchFileResponse) ProtoReflect() protoreflect.Message {
	mi := &file_import_file_service_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SearchFileResponse.ProtoReflect.Descriptor instead.
func (*SearchFileResponse) Descriptor() ([]byte, []int) {
	return file_import_file_service_proto_rawDescGZIP(), []int{7}
}

func (x *SearchFileResponse) GetData() []*model.RegistryFile {
	if x != nil {
		return x.Data
	}
	return nil
}

var File_import_file_service_proto protoreflect.FileDescriptor

var file_import_file_service_proto_rawDesc = []byte{
	0x0a, 0x19, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2f, 0x66, 0x69, 0x6c, 0x65, 0x2d, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x06, 0x69, 0x6d, 0x70,
	0x6f, 0x72, 0x74, 0x1a, 0x14, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x76, 0x65, 0x72, 0x73,
	0x69, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x11, 0x63, 0x6f, 0x6d, 0x6d, 0x6f,
	0x6e, 0x2f, 0x67, 0x72, 0x69, 0x64, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x20, 0x69, 0x6d,
	0x70, 0x6f, 0x72, 0x74, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x2f, 0x72, 0x65, 0x67, 0x69, 0x73,
	0x74, 0x72, 0x79, 0x2d, 0x66, 0x69, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1e,
	0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x2f, 0x73, 0x65, 0x61,
	0x72, 0x63, 0x68, 0x2d, 0x66, 0x69, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x68,
	0x0a, 0x12, 0x42, 0x65, 0x67, 0x69, 0x6e, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x1a, 0x0a, 0x08, 0x65, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x49, 0x64,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x08, 0x65, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x49, 0x64,
	0x12, 0x1a, 0x0a, 0x08, 0x73, 0x63, 0x68, 0x65, 0x6d, 0x65, 0x49, 0x64, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x08, 0x73, 0x63, 0x68, 0x65, 0x6d, 0x65, 0x49, 0x64, 0x12, 0x1a, 0x0a, 0x08,
	0x66, 0x69, 0x6c, 0x65, 0x4e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
	0x66, 0x69, 0x6c, 0x65, 0x4e, 0x61, 0x6d, 0x65, 0x22, 0x2d, 0x0a, 0x13, 0x42, 0x65, 0x67, 0x69,
	0x6e, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x16, 0x0a, 0x06, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x06, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x64, 0x22, 0x45, 0x0a, 0x17, 0x50, 0x72, 0x6f, 0x63, 0x65,
	0x73, 0x73, 0x69, 0x6e, 0x67, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x06, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x64, 0x61,
	0x74, 0x61, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x22, 0x1a,
	0x0a, 0x18, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x55, 0x70, 0x6c, 0x6f,
	0x61, 0x64, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x2c, 0x0a, 0x12, 0x43, 0x6c,
	0x6f, 0x73, 0x65, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x16, 0x0a, 0x06, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x06, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x64, 0x22, 0x31, 0x0a, 0x13, 0x43, 0x6c, 0x6f, 0x73,
	0x65, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x1a, 0x0a, 0x08, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x05, 0x52, 0x08, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x49, 0x64, 0x22, 0x61, 0x0a, 0x11, 0x53,
	0x65, 0x61, 0x72, 0x63, 0x68, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x20, 0x0a, 0x04, 0x67, 0x72, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0c,
	0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x47, 0x72, 0x69, 0x64, 0x52, 0x04, 0x67, 0x72,
	0x69, 0x64, 0x12, 0x2a, 0x0a, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x12, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x53, 0x65, 0x61, 0x72,
	0x63, 0x68, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x22, 0x3e,
	0x0a, 0x12, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x12, 0x28, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x03,
	0x28, 0x0b, 0x32, 0x14, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x52, 0x65, 0x67, 0x69,
	0x73, 0x74, 0x72, 0x79, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x32, 0xf1,
	0x02, 0x0a, 0x0b, 0x46, 0x69, 0x6c, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x3a,
	0x0a, 0x07, 0x56, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x12, 0x16, 0x2e, 0x63, 0x6f, 0x6d, 0x6d,
	0x6f, 0x6e, 0x2e, 0x56, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x17, 0x2e, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x56, 0x65, 0x72, 0x73, 0x69,
	0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x46, 0x0a, 0x0b, 0x42, 0x65,
	0x67, 0x69, 0x6e, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x12, 0x1a, 0x2e, 0x69, 0x6d, 0x70, 0x6f,
	0x72, 0x74, 0x2e, 0x42, 0x65, 0x67, 0x69, 0x6e, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1b, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x42,
	0x65, 0x67, 0x69, 0x6e, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x55, 0x0a, 0x10, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67,
	0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x12, 0x1f, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2e,
	0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74,
	0x2e, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x69, 0x6e, 0x67, 0x55, 0x70, 0x6c, 0x6f, 0x61,
	0x64, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x46, 0x0a, 0x0b, 0x43, 0x6c, 0x6f,
	0x73, 0x65, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x12, 0x1a, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72,
	0x74, 0x2e, 0x43, 0x6c, 0x6f, 0x73, 0x65, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x1b, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x43, 0x6c,
	0x6f, 0x73, 0x65, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x12, 0x3f, 0x0a, 0x06, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x12, 0x19, 0x2e, 0x69, 0x6d,
	0x70, 0x6f, 0x72, 0x74, 0x2e, 0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x46, 0x69, 0x6c, 0x65, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1a, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x2e,
	0x53, 0x65, 0x61, 0x72, 0x63, 0x68, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x42, 0x7a, 0x0a, 0x1a, 0x63, 0x6f, 0x6d, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x79,
	0x2d, 0x6e, 0x65, 0x74, 0x2e, 0x67, 0x72, 0x70, 0x63, 0x2e, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74,
	0x42, 0x0b, 0x46, 0x69, 0x6c, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x50, 0x01, 0x5a,
	0x2b, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x73, 0x75, 0x70, 0x70,
	0x6c, 0x79, 0x2d, 0x6e, 0x65, 0x74, 0x2f, 0x73, 0x68, 0x61, 0x72, 0x65, 0x64, 0x2f, 0x67, 0x6f,
	0x2d, 0x67, 0x72, 0x70, 0x63, 0x2f, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0xa2, 0x02, 0x03, 0x4d,
	0x58, 0x58, 0xaa, 0x02, 0x10, 0x53, 0x75, 0x70, 0x70, 0x6c, 0x79, 0x4e, 0x65, 0x74, 0x2e, 0x49,
	0x6d, 0x70, 0x6f, 0x72, 0x74, 0xca, 0x02, 0x06, 0x49, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x62, 0x06,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_import_file_service_proto_rawDescOnce sync.Once
	file_import_file_service_proto_rawDescData = file_import_file_service_proto_rawDesc
)

func file_import_file_service_proto_rawDescGZIP() []byte {
	file_import_file_service_proto_rawDescOnce.Do(func() {
		file_import_file_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_import_file_service_proto_rawDescData)
	})
	return file_import_file_service_proto_rawDescData
}

var file_import_file_service_proto_msgTypes = make([]protoimpl.MessageInfo, 8)
var file_import_file_service_proto_goTypes = []interface{}{
	(*BeginUploadRequest)(nil),       // 0: import.BeginUploadRequest
	(*BeginUploadResponse)(nil),      // 1: import.BeginUploadResponse
	(*ProcessingUploadRequest)(nil),  // 2: import.ProcessingUploadRequest
	(*ProcessingUploadResponse)(nil), // 3: import.ProcessingUploadResponse
	(*CloseUploadRequest)(nil),       // 4: import.CloseUploadRequest
	(*CloseUploadResponse)(nil),      // 5: import.CloseUploadResponse
	(*SearchFileRequest)(nil),        // 6: import.SearchFileRequest
	(*SearchFileResponse)(nil),       // 7: import.SearchFileResponse
	(*common.Grid)(nil),              // 8: common.Grid
	(*model.SearchFile)(nil),         // 9: import.SearchFile
	(*model.RegistryFile)(nil),       // 10: import.RegistryFile
	(*common.VersionRequest)(nil),    // 11: common.VersionRequest
	(*common.VersionResponse)(nil),   // 12: common.VersionResponse
}
var file_import_file_service_proto_depIdxs = []int32{
	8,  // 0: import.SearchFileRequest.grid:type_name -> common.Grid
	9,  // 1: import.SearchFileRequest.search:type_name -> import.SearchFile
	10, // 2: import.SearchFileResponse.data:type_name -> import.RegistryFile
	11, // 3: import.FileService.Version:input_type -> common.VersionRequest
	0,  // 4: import.FileService.BeginUpload:input_type -> import.BeginUploadRequest
	2,  // 5: import.FileService.ProcessingUpload:input_type -> import.ProcessingUploadRequest
	4,  // 6: import.FileService.CloseUpload:input_type -> import.CloseUploadRequest
	6,  // 7: import.FileService.Search:input_type -> import.SearchFileRequest
	12, // 8: import.FileService.Version:output_type -> common.VersionResponse
	1,  // 9: import.FileService.BeginUpload:output_type -> import.BeginUploadResponse
	3,  // 10: import.FileService.ProcessingUpload:output_type -> import.ProcessingUploadResponse
	5,  // 11: import.FileService.CloseUpload:output_type -> import.CloseUploadResponse
	7,  // 12: import.FileService.Search:output_type -> import.SearchFileResponse
	8,  // [8:13] is the sub-list for method output_type
	3,  // [3:8] is the sub-list for method input_type
	3,  // [3:3] is the sub-list for extension type_name
	3,  // [3:3] is the sub-list for extension extendee
	0,  // [0:3] is the sub-list for field type_name
}

func init() { file_import_file_service_proto_init() }
func file_import_file_service_proto_init() {
	if File_import_file_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_import_file_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BeginUploadRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_import_file_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BeginUploadResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_import_file_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ProcessingUploadRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_import_file_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ProcessingUploadResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_import_file_service_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CloseUploadRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_import_file_service_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CloseUploadResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_import_file_service_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SearchFileRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_import_file_service_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SearchFileResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_import_file_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   8,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_import_file_service_proto_goTypes,
		DependencyIndexes: file_import_file_service_proto_depIdxs,
		MessageInfos:      file_import_file_service_proto_msgTypes,
	}.Build()
	File_import_file_service_proto = out.File
	file_import_file_service_proto_rawDesc = nil
	file_import_file_service_proto_goTypes = nil
	file_import_file_service_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// FileServiceClient is the client API for FileService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type FileServiceClient interface {
	Version(ctx context.Context, in *common.VersionRequest, opts ...grpc.CallOption) (*common.VersionResponse, error)
	BeginUpload(ctx context.Context, in *BeginUploadRequest, opts ...grpc.CallOption) (*BeginUploadResponse, error)
	ProcessingUpload(ctx context.Context, in *ProcessingUploadRequest, opts ...grpc.CallOption) (*ProcessingUploadResponse, error)
	CloseUpload(ctx context.Context, in *CloseUploadRequest, opts ...grpc.CallOption) (*CloseUploadResponse, error)
	Search(ctx context.Context, in *SearchFileRequest, opts ...grpc.CallOption) (*SearchFileResponse, error)
}

type fileServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewFileServiceClient(cc grpc.ClientConnInterface) FileServiceClient {
	return &fileServiceClient{cc}
}

func (c *fileServiceClient) Version(ctx context.Context, in *common.VersionRequest, opts ...grpc.CallOption) (*common.VersionResponse, error) {
	out := new(common.VersionResponse)
	err := c.cc.Invoke(ctx, "/import.FileService/Version", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileServiceClient) BeginUpload(ctx context.Context, in *BeginUploadRequest, opts ...grpc.CallOption) (*BeginUploadResponse, error) {
	out := new(BeginUploadResponse)
	err := c.cc.Invoke(ctx, "/import.FileService/BeginUpload", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileServiceClient) ProcessingUpload(ctx context.Context, in *ProcessingUploadRequest, opts ...grpc.CallOption) (*ProcessingUploadResponse, error) {
	out := new(ProcessingUploadResponse)
	err := c.cc.Invoke(ctx, "/import.FileService/ProcessingUpload", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileServiceClient) CloseUpload(ctx context.Context, in *CloseUploadRequest, opts ...grpc.CallOption) (*CloseUploadResponse, error) {
	out := new(CloseUploadResponse)
	err := c.cc.Invoke(ctx, "/import.FileService/CloseUpload", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fileServiceClient) Search(ctx context.Context, in *SearchFileRequest, opts ...grpc.CallOption) (*SearchFileResponse, error) {
	out := new(SearchFileResponse)
	err := c.cc.Invoke(ctx, "/import.FileService/Search", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// FileServiceServer is the server API for FileService service.
type FileServiceServer interface {
	Version(context.Context, *common.VersionRequest) (*common.VersionResponse, error)
	BeginUpload(context.Context, *BeginUploadRequest) (*BeginUploadResponse, error)
	ProcessingUpload(context.Context, *ProcessingUploadRequest) (*ProcessingUploadResponse, error)
	CloseUpload(context.Context, *CloseUploadRequest) (*CloseUploadResponse, error)
	Search(context.Context, *SearchFileRequest) (*SearchFileResponse, error)
}

// UnimplementedFileServiceServer can be embedded to have forward compatible implementations.
type UnimplementedFileServiceServer struct {
}

func (*UnimplementedFileServiceServer) Version(context.Context, *common.VersionRequest) (*common.VersionResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Version not implemented")
}
func (*UnimplementedFileServiceServer) BeginUpload(context.Context, *BeginUploadRequest) (*BeginUploadResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method BeginUpload not implemented")
}
func (*UnimplementedFileServiceServer) ProcessingUpload(context.Context, *ProcessingUploadRequest) (*ProcessingUploadResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ProcessingUpload not implemented")
}
func (*UnimplementedFileServiceServer) CloseUpload(context.Context, *CloseUploadRequest) (*CloseUploadResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CloseUpload not implemented")
}
func (*UnimplementedFileServiceServer) Search(context.Context, *SearchFileRequest) (*SearchFileResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Search not implemented")
}

func RegisterFileServiceServer(s *grpc.Server, srv FileServiceServer) {
	s.RegisterService(&_FileService_serviceDesc, srv)
}

func _FileService_Version_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(common.VersionRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).Version(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/import.FileService/Version",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).Version(ctx, req.(*common.VersionRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileService_BeginUpload_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BeginUploadRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).BeginUpload(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/import.FileService/BeginUpload",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).BeginUpload(ctx, req.(*BeginUploadRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileService_ProcessingUpload_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ProcessingUploadRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).ProcessingUpload(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/import.FileService/ProcessingUpload",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).ProcessingUpload(ctx, req.(*ProcessingUploadRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileService_CloseUpload_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CloseUploadRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).CloseUpload(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/import.FileService/CloseUpload",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).CloseUpload(ctx, req.(*CloseUploadRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FileService_Search_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchFileRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FileServiceServer).Search(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/import.FileService/Search",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FileServiceServer).Search(ctx, req.(*SearchFileRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _FileService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "import.FileService",
	HandlerType: (*FileServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Version",
			Handler:    _FileService_Version_Handler,
		},
		{
			MethodName: "BeginUpload",
			Handler:    _FileService_BeginUpload_Handler,
		},
		{
			MethodName: "ProcessingUpload",
			Handler:    _FileService_ProcessingUpload_Handler,
		},
		{
			MethodName: "CloseUpload",
			Handler:    _FileService_CloseUpload_Handler,
		},
		{
			MethodName: "Search",
			Handler:    _FileService_Search_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "import/file-service.proto",
}

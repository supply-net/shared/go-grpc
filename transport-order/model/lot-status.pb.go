// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.21.0
// 	protoc        v3.11.4
// source: transport-order/model/lot-status.proto

package model

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type EnumLotStatus int32

const (
	EnumLotStatus_LOT_UNKNOWN   EnumLotStatus = 0
	EnumLotStatus_LOT_NEW       EnumLotStatus = 1
	EnumLotStatus_LOT_TRANSIT   EnumLotStatus = 2
	EnumLotStatus_LOT_DELIVERED EnumLotStatus = 3
)

// Enum value maps for EnumLotStatus.
var (
	EnumLotStatus_name = map[int32]string{
		0: "LOT_UNKNOWN",
		1: "LOT_NEW",
		2: "LOT_TRANSIT",
		3: "LOT_DELIVERED",
	}
	EnumLotStatus_value = map[string]int32{
		"LOT_UNKNOWN":   0,
		"LOT_NEW":       1,
		"LOT_TRANSIT":   2,
		"LOT_DELIVERED": 3,
	}
)

func (x EnumLotStatus) Enum() *EnumLotStatus {
	p := new(EnumLotStatus)
	*p = x
	return p
}

func (x EnumLotStatus) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (EnumLotStatus) Descriptor() protoreflect.EnumDescriptor {
	return file_transport_order_model_lot_status_proto_enumTypes[0].Descriptor()
}

func (EnumLotStatus) Type() protoreflect.EnumType {
	return &file_transport_order_model_lot_status_proto_enumTypes[0]
}

func (x EnumLotStatus) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use EnumLotStatus.Descriptor instead.
func (EnumLotStatus) EnumDescriptor() ([]byte, []int) {
	return file_transport_order_model_lot_status_proto_rawDescGZIP(), []int{0}
}

var File_transport_order_model_lot_status_proto protoreflect.FileDescriptor

var file_transport_order_model_lot_status_proto_rawDesc = []byte{
	0x0a, 0x26, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2d, 0x6f, 0x72, 0x64, 0x65,
	0x72, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x2f, 0x6c, 0x6f, 0x74, 0x2d, 0x73, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70,
	0x6f, 0x72, 0x74, 0x4f, 0x72, 0x64, 0x65, 0x72, 0x2a, 0x51, 0x0a, 0x0d, 0x45, 0x6e, 0x75, 0x6d,
	0x4c, 0x6f, 0x74, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x0f, 0x0a, 0x0b, 0x4c, 0x4f, 0x54,
	0x5f, 0x55, 0x4e, 0x4b, 0x4e, 0x4f, 0x57, 0x4e, 0x10, 0x00, 0x12, 0x0b, 0x0a, 0x07, 0x4c, 0x4f,
	0x54, 0x5f, 0x4e, 0x45, 0x57, 0x10, 0x01, 0x12, 0x0f, 0x0a, 0x0b, 0x4c, 0x4f, 0x54, 0x5f, 0x54,
	0x52, 0x41, 0x4e, 0x53, 0x49, 0x54, 0x10, 0x02, 0x12, 0x11, 0x0a, 0x0d, 0x4c, 0x4f, 0x54, 0x5f,
	0x44, 0x45, 0x4c, 0x49, 0x56, 0x45, 0x52, 0x45, 0x44, 0x10, 0x03, 0x42, 0xa1, 0x01, 0x0a, 0x25,
	0x63, 0x6f, 0x6d, 0x2e, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x79, 0x2d, 0x6e, 0x65, 0x74, 0x2e, 0x74,
	0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x42, 0x0b, 0x54, 0x72, 0x69, 0x70, 0x53, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x50, 0x01, 0x5a, 0x3a, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d,
	0x2f, 0x73, 0x75, 0x70, 0x70, 0x6c, 0x79, 0x2d, 0x6e, 0x65, 0x74, 0x2f, 0x73, 0x68, 0x61, 0x72,
	0x65, 0x64, 0x2f, 0x67, 0x6f, 0x2d, 0x67, 0x72, 0x70, 0x63, 0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73,
	0x70, 0x6f, 0x72, 0x74, 0x2d, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c,
	0xa2, 0x02, 0x03, 0x4d, 0x58, 0x58, 0xaa, 0x02, 0x18, 0x53, 0x75, 0x70, 0x70, 0x6c, 0x79, 0x4e,
	0x65, 0x74, 0x2e, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x4f, 0x72, 0x64, 0x65,
	0x72, 0xca, 0x02, 0x0b, 0x54, 0x72, 0x69, 0x70, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62,
	0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_transport_order_model_lot_status_proto_rawDescOnce sync.Once
	file_transport_order_model_lot_status_proto_rawDescData = file_transport_order_model_lot_status_proto_rawDesc
)

func file_transport_order_model_lot_status_proto_rawDescGZIP() []byte {
	file_transport_order_model_lot_status_proto_rawDescOnce.Do(func() {
		file_transport_order_model_lot_status_proto_rawDescData = protoimpl.X.CompressGZIP(file_transport_order_model_lot_status_proto_rawDescData)
	})
	return file_transport_order_model_lot_status_proto_rawDescData
}

var file_transport_order_model_lot_status_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_transport_order_model_lot_status_proto_goTypes = []interface{}{
	(EnumLotStatus)(0), // 0: transportOrder.EnumLotStatus
}
var file_transport_order_model_lot_status_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_transport_order_model_lot_status_proto_init() }
func file_transport_order_model_lot_status_proto_init() {
	if File_transport_order_model_lot_status_proto != nil {
		return
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_transport_order_model_lot_status_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   0,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_transport_order_model_lot_status_proto_goTypes,
		DependencyIndexes: file_transport_order_model_lot_status_proto_depIdxs,
		EnumInfos:         file_transport_order_model_lot_status_proto_enumTypes,
	}.Build()
	File_transport_order_model_lot_status_proto = out.File
	file_transport_order_model_lot_status_proto_rawDesc = nil
	file_transport_order_model_lot_status_proto_goTypes = nil
	file_transport_order_model_lot_status_proto_depIdxs = nil
}
